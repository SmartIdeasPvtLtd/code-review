Helpful points for Code Review:
Take code review as a step to learn from others’ code and help other to improve. Adopt positive attitude.
Do not review code for longer than 1 Hours. If taking longer take short break or move to other work and continue later.
(if possible) Try to minimize code length around 500 lines for review.
Key points to review
Syntax – Is the code well formatted, meeting indentation and whitespace standards and free from parse errors?
Style – Is the code idiomatic for the language, are common error-prone patterns for that language avoided, is the code easily understood?
Functional – Does the code do what it’s supposed to do?
Architecture – Is the code required, are there any improvements to be made through abstractions, reuse of other code. Does the code tie in with the rest of the codebase?
If code review is assigned to you, find earliest possible free time for the review. Don’t keep it pending for too long.
Developer should not force hard and fast time to review the code.
Keep in mind the review is not a battle for supremacy. This is a process of learning and helping, none of us is inferior/superior than others.
It is okay if you don’t find any problem in the code all the time.
If a reviewer shows some problems in code, (for submitter) don’t take it personally, (for reviewer) do not direct criticism in personal level.
It is known to be easier for reviewer to maintain a checklist of what to review.
If both submitter and reviewer is free, it is encouraged to do pair review.
You should never hesitate to commit/push code in sub-branches, it is better to push error code than losing the code.

Learn Git
We all know push/pull in Git but from now on we are going to use much deeper features of Git & BitBucket
Multi branch workflow
Branch Merging - learn here
Pull Request - learn here

Rules to be followed:
No codes should be written in Master branch, any commits in master must be done by Team Leader.
Must have dev branch in the repo, keep it clean and should be in line with master branch.
Every change are done in its specific branch no matter small or big task.
Convention for branch naming
feature/<feature-name> - for new feature addition
bugfix/<name-the-bug> - for bugfix
hotfix/<name-the-fix> - for hotfix
While branching each branch should be created from dev
Make a habit of adding code comments. Function level code comment is MUST ie at least explain what is the function of the function.
Before submitting for code review, developer should self review code.
Reviewer must have clear knowledge of requirements for reviewing code and access to the code in BitBucket.
The code review must be done in BitBucket on pull request
Submitter creates a pull request for the feature branch to dev branch  and assigns it to reviewer. In addition, the submitter must notify reviewer about the work and branch name.
Reviewer must use Bitbucket’s pull request by submitter to review the code, add comments, give proper explanation, approve and disapprove the pull request.
Repeat code review till reviewer approves.
When code is approved it is merged in dev branch. Select remove feature branch if task is completed (optional).
Feature branch must be reviewed
All bug fixes are reviewed before release
A separate branch (bugreview) is maintained for bug review
After each bug fix commit/merge it to bugreview branch
Multiple bug fixes will be reviewed at once and pull request to dev during release time
All hotfix is self reviewed (rubber duck review)
For hotfix branch can be done from master
Merge to master must be done via pull request
Hotfix pull request should be approved by Team Leader during office hours.
If hotfix is done at off hours, pull request is still required but can be self approved.
Only approve the pull request if there is no need for changes in the code.
Once review is approved, developers create pull request from working branch to dev branch. Anyone in team can approve this pull request.
Merge conflict should be resolved by involved developers.
Before release, dev branch to master branch merge must be done by Team Leader only. With proper branching and merging of code, the chance of merge conflict should be nil. In case of conflict resolve, resolution should be done by Team Leader with help from developers.
Master and dev branch will have automated build in docker hub.
Commit/merge to master must have version number
Auto build from master have docker image tagged v<version-name>
Auto build from dev have docker image tagged dev
If the process doesn’t exist already, Team Leader make sure of it. If they don’t have access to docker hub, consult Amit Joshi or Shankar Uprety.

Notes for Team Leader:
It is Team Leader’s task to make sure branch pattern is followed and suitable access is provided to developers
Amit Joshi will continuously monitor if code review is being done and rules are followed.
If code review is being skipped Team Leader will be informed, leader may need to provide clarification for lack of it.
Further notice about code-review will provided in code-review channel of Slack. Make sure everyone is in code-review channel of Slack.
For further queries on code review contact Amit Joshi @ ajoshi@hamropatro.com or Slack.

We hope everyone will perform their task accordingly, learn something new and it will help us grow personally and technically.
